SRC = src
BUILD = build
FLAGS = -g -O2 -std=gnu99 -Wall -Wextra -Wpedantic
LIBS = -lGLEW -lglfw -lGL -lm
OBJ = $(patsubst $(SRC)/%.c,$(BUILD)/%.o,$(wildcard $(SRC)/*.c))
INC = $(wildcard src/*.h)

all: $(BUILD) main

run: all
	./main

clean:
	rm -Rf $(BUILD)
	rm main

$(BUILD):
	mkdir -p $@

$(BUILD)/%.o: $(SRC)/%.c ${INC}
	gcc -c -o $@ $(FLAGS) $<

main: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)
