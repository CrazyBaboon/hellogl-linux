#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "matrix.h"
#include "shader.h"
#include "util.h"

int main(void) {

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    if (!glfwInit()) {
        return -1;
    }

    GLFWwindow *window = glfwCreateWindow(800, 800, "HelloGL", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    if (glewInit() != GLEW_OK) {
        glfwTerminate();
        return -1;
    }

    GLuint program = load_program("shaders/vertex.glsl", "shaders/fragment.glsl");
    GLuint program_lines = load_program("shaders/vertex.glsl", "shaders/fragment_lines.glsl");
    GLuint position = glGetAttribLocation(program, "position");
    GLuint position_lines = glGetAttribLocation(program_lines, "position");
    GLuint matrix = glGetUniformLocation(program, "matrix");
    GLuint matrix_lines = glGetUniformLocation(program_lines, "matrix");


    float data[] = {
    -1.0f,-1.0f,-1.0f, // triangle 1 : begin
    -1.0f,-1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f, // triangle 1 : end
    1.0f, 1.0f,-1.0f, // triangle 2 : begin
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f, // triangle 2 : end
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,

    -1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f
    };

    float data_lines[] = {

        /* top face edges */
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,

    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,

    -1.0f,-1.0f, 1.0f,
    -1.0f,1.0f, 1.0f,

-1.0f,1.0f, 1.0f,
1.0f,1.0f, 1.0f,

/* top - down edges */
    -1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, -1.0f,

-1.0f,1.0f, 1.0f,
-1.0f,1.0f, -1.0f,

1.0f,-1.0f, 1.0f,
1.0f,-1.0f, -1.0f,

1.0f,1.0f, -1.0f,
1.0f,1.0f, 1.0f,

/* bottom face edges */
    1.0f, 1.0f, -1.0f,
    1.0f,-1.0f, -1.0f,

    1.0f,-1.0f, -1.0f,
    -1.0f,-1.0f, -1.0f,

    -1.0f,-1.0f, -1.0f,
    -1.0f,1.0f, -1.0f,

-1.0f,1.0f, -1.0f,
1.0f,1.0f, -1.0f

    };

/* Create custom framebuffer */
unsigned int fbo;
glGenFramebuffers(1, &fbo);
glBindFramebuffer(GL_FRAMEBUFFER, fbo);  

/* Create texture to attach to off-screen framebuffer */ 
unsigned int texture;
glGenTextures(1, &texture);
glBindTexture(GL_TEXTURE_2D, texture);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  

/* Assign the texture to the off-screen framebuffer */
glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

/* Create a stencil and depth image and attach it to the off-screen buffer */
glTexImage2D(
  GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 800, 600, 0, 
  GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL
);
glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, texture, 0);  


/* Check if the framebuffer is complete */ 
if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
{printf("Coninha!\n");}

/* unbind off-screen buffer */
glBindFramebuffer(GL_FRAMEBUFFER, 0);
/* Delete custom framebuffer */
glDeleteFramebuffers(1, &fbo); 

/* Render scene back to default framebuffer */
glBindFramebuffer(GL_FRAMEBUFFER, 0); 

    GLuint buffer = gen_buffer(sizeof(data), data);
    GLuint buffer_lines = gen_buffer(sizeof(data_lines), data_lines);

    while (!glfwWindowShouldClose(window)) {

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float mat[16];
        mat_identity(mat);
        mat_translate(mat, 0, -0.5, 0);
        mat_rotate(mat, 0.1, 0.3, 1, glfwGetTime());
        mat_ortho(mat, -2, 2, -2, 2, -2, 2);
        
        /* draw triangles */
        glUniformMatrix4fv(matrix, 1, GL_FALSE, mat);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glEnableVertexAttribArray(position);
        glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glUseProgram(program);
        glDrawArrays(GL_TRIANGLES, 0, 12*3);

        /* draw lines */
        glUniformMatrix4fv(matrix_lines, 1, GL_FALSE, mat);
        glBindBuffer(GL_ARRAY_BUFFER, buffer_lines);
        glEnableVertexAttribArray(position_lines);
        glVertexAttribPointer(position_lines, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glUseProgram(program_lines);
        glLineWidth(3.0f);
        glDrawArrays(GL_LINES, 0, 12*2);

        glDisableVertexAttribArray(position);
        glDisableVertexAttribArray(position_lines);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
